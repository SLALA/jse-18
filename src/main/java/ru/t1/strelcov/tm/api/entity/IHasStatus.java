package ru.t1.strelcov.tm.api.entity;

import ru.t1.strelcov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
