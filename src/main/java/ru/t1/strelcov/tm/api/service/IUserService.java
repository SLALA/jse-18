package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User add(String login, String password);

    User add(String login, String password, String email);

    User add(String login, String password, Role role);

    void add(User user);

    void clear();

    void remove(User user);

    User findById(String id);

    User findByLogin(String name);

    User removeById(String id);

    User removeByLogin(String name);

    User updateById(String id, String firstName, String lastName, String middleName, String email);

    User updateByLogin(String login, String firstName, String lastName, String middleName, String email);

    void changePasswordById(String id, String password);

}
