package ru.t1.strelcov.tm.api.repository;

import ru.t1.strelcov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    void add(User user);

    void clear();

    void remove(User user);

    User findById(String id);

    User findByLogin(String login);

    User removeById(String id);

    User removeByLogin(String login);

}
