package ru.t1.strelcov.tm.exception.empty;

import ru.t1.strelcov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error: Password is empty.");
    }

}
