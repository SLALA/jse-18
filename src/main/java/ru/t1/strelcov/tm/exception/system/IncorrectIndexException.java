package ru.t1.strelcov.tm.exception.system;

import ru.t1.strelcov.tm.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(final String value) {
        super("Error: Input value \"" + value + "\" is not a number.");
    }

    public IncorrectIndexException() {
        super("Error: Index is incorrect.");
    }

}
