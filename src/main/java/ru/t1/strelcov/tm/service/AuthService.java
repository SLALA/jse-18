package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.service.IAuthService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.exception.empty.EmptyLoginException;
import ru.t1.strelcov.tm.exception.empty.EmptyPasswordException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.IncorrectPasswordException;
import ru.t1.strelcov.tm.exception.entity.UserNotFoundException;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (userId == null) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final String passwordHash = HashUtil.salt(password);
        final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (!user.getPasswordHash().equals(passwordHash)) throw new IncorrectPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        if (userId == null) throw new AccessDeniedException();
        userId = null;
    }

}
