package ru.t1.strelcov.tm.command.project;

import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.COMPLETED;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-complete-by-index";
    }

    @Override
    public String description() {
        return "Complete project by index.";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeStatusByIndex(index, COMPLETED);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
