package ru.t1.strelcov.tm.command.system;

import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

public class DisplayCommands extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Display commands.";
    }

    @Override
    public void execute() {
        final ICommandService commandService = serviceLocator.getCommandService();
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = commandService.getCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
